package thread;
import java.util.Scanner;
/**
 * 线程提供了一个静态方法:
 * static void sleep(long ms)
 * 该方法可以让线程进入阻塞状态指定毫秒,超过后线程会自动回到RUNNABLE状态再次并发执行
 */
public class SleepDemo {
    public static void main(String[] args){
        /*
            倒计时程序
            程序启动后输入一个数字,然后从该数字开始每秒递减,到0时输出时间到
            程序退出
         */
        System.out.println("程序开始了....");
        Scanner sca = new Scanner(System.in);
        System.out.println("输入一个数字:");
        for(int t  = sca.nextInt();t>0;t--){

            System.out.println(t);
            try{

                Thread.sleep(1000);

            }catch(InterruptedException e){
                e.printStackTrace();
            }
        }

        System.out.println("结束了");
    }
}
