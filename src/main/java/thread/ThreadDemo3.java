package thread;

/**
 * 使用匿名内部类方式完成两种线程的创建方式
 */
public class ThreadDemo3 {
    public static void main(String[] args) {
        //继承Thread重写run方法的方式
        Thread t1 = new Thread(){
            public void run(){
                for(int i = 0;i<200;i++){
                    System.out.println("hahaha a!");
                }
            }
        };
        //实现Runnable接口单独定义线程任务的方式
        Runnable r2 = ()->{
            for(int i = 0;i<200;i++){
                System.out.println("好家伙!");
            }
        };
        Thread t2  = new Thread(r2);
        t1.start();
        t2.start();
    }
}
