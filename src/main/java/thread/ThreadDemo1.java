package thread;

/**
 * 多线程
 *Thread 线程,线程是指一个程序单一的顺序执行流程
 * 多线程:多个单一的顺序执行流程 同时 执行
 *
 * 多线程的执行实际上是并发的,CPU在这些线程(流程)之间快速切换执行,由于速度快造就了一种
 * 感官上的同时运行效果,因此实际上微观上是走走停停的,宏观上是一起动的效果.
 *
 * 第一种创建线程的方式:
 * 1.定义一个类并继承Thread,用来定义一个执行流程
 * 2.重写run方法,用来定义这个流程中需要运行的所有代码
 * 3.调用线程的start方法将这个流程跑起来,那么这个流程就会和其他流程并发运行
 */
public class ThreadDemo1 {
    public static void main(String[] args) {
        //实例化两个线程(两个流程)
        MyThread1 th = new MyThread1();
        MyThread2 th2 = new MyThread2();
        th.start();
        th2.start();

    }

}
/*
    继承Thread并重写run方法的优点在于结构简单,匿名内部类方式创建推荐这种方式
    缺点有两个:
    1.由于java是单继承的,这会导致如果继承了Thread就无法再继承其他类去复用方法了
    实际开发中很不方便
    2.直接重写run方fa在线程中导致线程与线程实际执行的任务绑定在一起,不利于线程的重用.
 */
class MyThread1 extends Thread{
    public void run(){
        for(int i = 0;i<100;i++){
            System.out.println("铠甲合体");
        }
    }
}
class MyThread2 extends Thread{
    public void run(){
        for(int i = 0;i<100;i++){
            System.out.println("赛文!赛文!赛文~");
        }
    }
}
