package thread;

/**
 * 线程的优先级
 * 线程的10个优先级,分别对应整数1-10.其中1最低,10最高,5位默认
 * 线程启动后便纳入到线程调度器中统一管理,线程不能主动索取时间片,只能被动被分配,
 * 调度器会尽可能均匀的分配时间片给每个线程.调度线程的优先级可以改善一个线程获取时间
 * 片的几率
 * 线程优先级越高的线程获取时间片的次数越多
 * 由于现在cup为多核心,分配到不同核心上的线程优先级不起作用
 */
public class PriorityDemo {
    public static void main(String[] args) {
        Thread min = new Thread(){
            public void run(){
                for(int i = 0;i<1000;i++){
                    System.out.println("min");
                }
            }
        };
        Thread norm = new Thread(){
            public void run(){
                for(int i = 0;i<1000;i++){
                    System.out.println("norm");
                }
            }
        };
        Thread max = new Thread(){
            public void run(){
                for(int i = 0;i<1000;i++){
                    System.out.println("max");
                }
            }
        };
        /*
            优先级可以使用Thread指定的常量
            MIN_PRIORITY表示最小优先级,对应整数1
            MAX_PRIORITY表示最大优先级,对应整数10
            设置的优先级必须是1-10之间的整数,超过会抛出异常
         */
        min.setPriority(1);
        max.setPriority(9);
        min.start();
        max.start();
        norm.start();
    }
}
