package thread;

/**
 * 多线程并发安全问题
 * 当多个线程并发操作同一临界资源,由于线程切换时机不确定,导致操作顺序出现换乱而产生的
 * bug严重时可能会出现系统瘫痪的情况
 * 临界资源:操作该资源的完整流程同时只能被单一线程进行的资源.
 */
public class SyncDemo {
    public static void main(String[] args) {
        Table table = new Table();
        Thread t1 = new Thread(){
            public void run(){
                while(true){
                    int bean = table.getBean();
                    Thread.yield();
                    System.out.println(getName()+":"+bean);
                }
            }
        } ;
        Thread t2 = new Thread(){
            public void run(){
                while(true){
                    int bean = table.getBean();
                    Thread.yield();
                    System.out.println(getName()+":"+bean);
                }
            }
        } ;
        t1.start();
        t2.start();


    }

}
class Table{
    private int beans = 20;//桌子上有20豆子
    /*
        当一个方法上使用synchronized修饰后,这个方法称为同步方法,即:多个线程不能同时在
        该方法内部执行.
        将多个线程并发操作改为排队同步操作可有效解决多线程并发安全问题.
     */
    public synchronized int getBean(){
        if(beans==0){
            throw new RuntimeException("没有豆子了!");
        }
        Thread.yield();//让执行该方法的线程主动放弃本次剩余时间片,模拟执行到这里没有时间了
        return beans--;
    }

}
