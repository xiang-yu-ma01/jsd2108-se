package thread;

/**
 * 有效的缩小同步范围可以保证并发安全的前提下尽可能的提高并发效率
 *
 * 同步块
 * synchronized(同步监视器对象){
 *     需要多个线程同步执行的代码片段
 * }
 */
public class SyncDemo2 {
    public static void main(String[] args) {
        Shop shop = new Shop();
        Thread t1 = new Thread(){
            public void run(){
                shop.buy();
            }
        };
        Thread t2 = new Thread(){
            public void run(){
                shop.buy();
            }
        };
        t1.start();
        t2.start();
    }
}
class Shop{
    /*
        在方法上使用synchronized,那么同步监视器对象就是this,且不能自行指定.
     */
    //public synchronized void buy(){
    public void buy(){
        try{
            Thread t = Thread.currentThread();
            System.out.println(t.getName()+":正在挑衣服...");
            Thread.sleep(5000);
            /*
                同步块使用时要注意同步监视器对象的选择,要求:
                1.该对象可以是java中的任何引用类型的实例
                2.必须保证多个线程看到的该对象是同一个!
             */
            synchronized(this){
                //synchronized(new Object()){//不行

                //synchronized(t){//不行
                System.out.println(t.getName()+":正在试衣服...");
                Thread.sleep(5000);
            }
            System.out.println("结账");

        }catch(InterruptedException e){

        }
    }
}