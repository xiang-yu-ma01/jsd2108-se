package thread;

/**
 * 像sleep这样的方法会导致线程进入阻塞状态,该方法要求必须处理中断异常:
 * InterruptedException
 * 当一个线程调用sleep处于阻塞的过程中该线程的interrupt方法被调用了,那么就会立即中断该
 * 线程的阻塞,此时sleep方法会抛出异常.
 */
public class SleepDemo2 {
    public static void main(String[] args) {
        Thread lin = new Thread(){
            public void run(){
                System.out.println("林:睡一会....");
                try{
                    Thread.sleep(10000000);
                }catch(InterruptedException e){
                    System.out.println("林:QNMD,干嘛呢!");
                }
                System.out.println("醒了");
            }

        };
        Thread huang = new Thread(){
            public void run(){
                System.out.println("黄:大锤80,小锤40");
                for (int i = 0;i<5;i++){
                    System.out.println("黄:80!");
                    try{
                        Thread.sleep(1000);

                    }catch(InterruptedException e){}
                }
                System.out.println("蹦!!");
                System.out.println("黄:大哥,搞定!");
                lin.interrupt();//中断lin的睡眠阻塞
            }
        };
        lin.start();
        huang.start();
    }
}
