package thread;

/**
 * 互斥锁
 * 当使用多个synchronized锁定多个代码片段，并且指定的锁对象相同时，这些代码
 * 片段就是互斥的
 * 多个线程不能同时执行他们
 */
public class SyncDemo4 {
    public static void main(String[] args) {
        Foo foo = new Foo();
        Thread t1 = new Thread(
                ()->{
                    foo.methodA();
                }
        );
        Thread t2 = new Thread(
                ()->{
                    foo.methodB();
                }
        );
        t1.start();
        t2.start();
    }
}
class Foo{
    public synchronized void methodA(){
        Thread t = Thread.currentThread();
        System.out.println(t.getName()+":正在执行A程序。。。");
        try{
            Thread.sleep(5000);
        }catch(InterruptedException e){}
        System.out.println(t.getName()+":A方法执行完毕.");
    }
    public synchronized void methodB(){
        Thread t = Thread.currentThread();
        System.out.println(t.getName()+":正在执行B程序。。。");
        try{
            Thread.sleep(5000);
        }catch(InterruptedException e){}
        System.out.println(t.getName()+":B方法执行完毕.");
    }
}
