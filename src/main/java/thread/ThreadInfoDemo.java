package thread;

/**
 * 获取线程信息的相关方法
 */
public class ThreadInfoDemo {
    public static void main(String[] args) {
        //获取主线程
        Thread main =Thread.currentThread();
        //获取线程名字
        String name = main.getName();
        System.out.println("name:"+name);
        //获取线程的ID,唯一标识的特点:非空且唯一
        long id = main.getId();
        System.out.println("唯一标识:"+id);
        //获取线程的优先级,对应的整数1~10
        int priority =  main.getPriority();
        System.out.println("优先级:"+priority);
        //线程是否还活着
        boolean isAlive = main.isAlive();
        System.out.println("isAlive:"+isAlive);
        //是否为守护线程
        boolean isDaemon = main.isDaemon();
        System.out.println("isDemon:"+isDaemon);
        //是否被中断了
        boolean isInterrupted = main.isInterrupted();
        System.out.println("isInterrupted:"+isInterrupted);

    }
}
