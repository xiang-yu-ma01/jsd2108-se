package thread;

/**
 * 静态方法上若使用synchronized,则方法一定具有同步效果
 * 注:静态方法上指定的同步监视器对象不是this,而是当前的类对象
 * 类对象:
 * Class类的一个实例,JVM加载一个类时就会实例化一个Class的实例并用于保存加载的这个类的
 * 相关信息,因此每个被加载的类都有且只有一个Class的实例与之对应,静态方法的锁对象就是它
 * ;类对象会在后续反射知识点详细说明.
 */
public class SyncDemo3 {
    public static void main(String[] args) {
        Thread t1 = new Thread(()->Boo.dosome());
        Thread t2 = new Thread(()->Boo.dosome());
        t1.start();
        t2.start();

    }
}
class Boo{
   //public  synchronized static void dosome(){
    public static void dosome(){
        //静态方法中使用同步块则指定的锁对象也应当是当前类的对象,格式:类名.class
        synchronized(Boo.class){
            Thread t = Thread.currentThread();
            System.out.println(t.getName()+":正在执行dosome方法");
            try{
                Thread.sleep(5000);
            }catch(InterruptedException e){

            }
            System.out.println(t.getName()+"方法执行完毕!");
        }

    }
}
