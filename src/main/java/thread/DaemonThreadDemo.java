package thread;

/**
 * 守护线程
 *
 * 守护线程是通过普通线程调用setDaemon方法设置而来的
 * 守护线程在结束时机上与普通线程有一个区别在于:进程的结束
 * 当一个java进程中的所有普通线程都结束时,该进程就会结束,
 * 此时会强制杀死所有正在执行的守护线程.
 */
public class DaemonThreadDemo {
    public static void main(String[] args) {
        Thread rose = new Thread(){
          public void run(){
              for(int i=0;i<5;i++){
                  System.out.println("rose:let me go!");
                  try{
                      Thread.sleep(1000);

                  }catch(InterruptedException e){

                  }
              }
              System.out.println("rose:呀!");
              System.out.println("扑通!");
          }
        };
        Thread jack = new Thread(){
            public void run(){
                while(true){
                    System.out.println("jack:you jump,i jump!");
                    try{
                        Thread.sleep(1000);
                    }catch(InterruptedException e){

                    }
                }
            }
        };
        rose.start();
        /*
            设置守护线程的工作必须在该线程启动前进行,否则会出异常
         */
        jack.setDaemon(true);
        jack.start();
        //如果主线程不结束,那么进程就不会在rose线程结束时杀掉守护jack了
        //while(true);

    }
}
