package thread;

/**
 * join方法可以协调线程之间的同步运行
 * 同步运行:多个线程执行代码时出现了先后顺序的运行方式
 * 异步运行:多个线程执行代码各干各的.多线程本身就是异步运行
 */
public class joinDemo {
    private static boolean isFinish = false;//图片是否下载完毕
    public static void main(String[] args){
        Thread download = new Thread(){
            public void run(){
                for(int i=1;i<=100;i++){
                    System.out.println("doen:"+i+"%");
                    try{
                        Thread.sleep(10);
                    }catch(InterruptedException e){}
                }
                System.out.println("down:图片下载完毕!");
                isFinish = true;
            }

        };
        Thread show = new Thread(){
          public void run(){
              try{
                  System.out.println("show:开始显示文字...");
                  Thread.sleep(2000);
                  System.out.println("show:文字加载完毕!");
                  //让show线程等待download执行完毕后再继续
                  download.join();
                  System.out.println("show:开始显示图片...");
                  if(!isFinish){
                      throw new RuntimeException("图片加载失败!");
                  }
                  System.out.println("show:图片显示完毕!");
              }catch(InterruptedException e){

              }
          }
        };

        download.start();
        show.start();

    }
}
