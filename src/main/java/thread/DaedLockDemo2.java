package thread;

/**
 * 死锁
 * 解决死锁现象应当注意的原则就是:避免一个线程在持有一个锁的同时申请持有另一个锁!
 */
public class DaedLockDemo2 {
    private static Object chopsticks = new Object();//筷子
    private static Object spoon = new Object();//勺子

    public static void main(String[] args) {
        Thread np = new Thread(
                ()->{
                    try{
                        System.out.println("北方人开始吃饭!");
                        System.out.println("北方人去拿筷子..");
                        synchronized(chopsticks){
                            System.out.println("北方人开始拿筷子吃饭");
                            Thread.sleep(5000);
                            System.out.println("北方人吃完饭去拿勺子...");
                            Thread.sleep(5000);
                            System.out.println("北方人吃完了饭!");

                        }
                        System.out.println("北方人放下了筷子,去拿勺子");
                        synchronized(spoon){
                            System.out.println("北方人拿起勺子开始喝汤...");
                            Thread.sleep(5000);
                        }
                        System.out.println("北方人放下了勺子...");
                        System.out.println("北方人吃饭完毕");

                    }catch(InterruptedException e){}
                }
        );
        Thread sp = new Thread(
                ()->{
                    try{
                        System.out.println("南方人开始吃饭!");
                        System.out.println("南方人去拿勺子..");
                        synchronized(chopsticks){
                            System.out.println("南方人拿起了勺子喝汤...");
                            Thread.sleep(5000);
                            System.out.println("南方人喝完汤去拿筷子...");
                            Thread.sleep(5000);
                            synchronized(spoon){
                                System.out.println("南方人拿起筷子吃饭...");
                                Thread.sleep(5000);
                            }
                            System.out.println("南方人放下了筷子...");
                        }
                        System.out.println("南方人放下了勺子,吃饭完毕");

                    }catch(InterruptedException e){}
                }
        );
        sp.start();
        np.start();
    }
}

