package thread;

/**
 * d第二种创建线程的方式:
 * 单独实现Runnable接口定义线程任务
 */
public class TreadDemo2 {
    public static void main(String[] args) {
        Runnable r = new MyRunnable();
        Runnable r1 = new MyRunnable1();
        Thread t = new Thread(r);
        Thread t1 =new Thread(r1);
        t.start();
        t1.start();

    }
}

class MyRunnable implements Runnable{
    public void run(){
        for(int i = 0;i<100;i++){
            System.out.println("死吧你!");
        }
    }
}
class MyRunnable1 implements  Runnable{
    public void run(){
        for(int i = 0;i<100;i++){
            System.out.println("好的");
        }
    }
}
