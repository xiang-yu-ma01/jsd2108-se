package thread;

/**
 * java中所有的代码都是靠线程执行的，main方法也不例外，jvm启动后
 * 会创建一条线程来执行main方法，该线程的名字叫做“main”，所以通常
 * 它被称为“主线程”
 * 我们自己定义的线程在不指定名字的情况下系统会分配一个名字，格式为”thread-x“
 * （x是一个数）
 *
 * Thread提供一个静态方法：
 * static Thread currentThread（）
 * 获取执行该方法的线程
 */
public class CurrentThreadDemo {
    public static void main(String[] args) {
        /*
            后期会学习到一个很重要的API:ThreadLocal,他可以使得我们在一个线程上
            跨越多个方法时共享数据使用,其内部要用到currentThread方法来辨别线程
            如:spring的食物控制就是靠ThreadLocal实现的.
         */
        Thread main = Thread.currentThread();//获取执行main方法发的线程(主线程)
        System.out.println("线程:"+main);
        dosome();

    }
    public static void dosome(){
        Thread t = Thread.currentThread();//获取执行dosome方法的线程
        System.out.println("执行dosome方法的线程:"+t);
    }

}
