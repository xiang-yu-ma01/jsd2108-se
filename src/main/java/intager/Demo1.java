package intager;

import object.Point;

/**
 * 包装类
 * java有两大类型:基本类型和引用类型
 * 区别:
 * 基本类型直接以值的形式存在
 * 引用类型则以对象的形式存在
 *
 * 基本类型不具备面向对象特性(封装，继承，多态)。实际开发时多有不便。为了解决这个问题，
 * java为8个基本类型提供了包装类，目的是让基本类型以对象形式存在来参与面向对象开发。
 */
public class Demo1 {
    public static void main(String[] args) {
        //将int值以对象的形式表示  基本类型->包装类(引用类型)
        int d = 123;
        //1:使用构造方法(不推荐)
        Integer i1 = new Integer(d);
        Integer i2 = new Integer(d);
        System.out.println(i1==i2);//false
        System.out.println(i1.equals(i2));//true
        //2:包装类提供了静态方法valueOf可以将基本类型转换为包装类(推荐)
        Integer i3 = Integer.valueOf(d);//整数类型会重用-128到127之间的整数对象
        Integer i4 = Integer.valueOf(d);
        System.out.println(i3==i4);//true
        System.out.println(i3.equals(i4));//true

        //3:JDK5之后有自动拆装箱特性，可以在基本类型与引用类型间直接赋值
        Integer i5 = d;//触发了JDK5之后编译器的特性:自动装箱
        //上述代码会被编译器改为:Integer i5 = Integer.valueOf(d);
        Double dou = 123.123;//Double dou = Double.valueOf(123.123);

        //包装类还原为基本类型
//        int s = i5.intValue();
        int s = i5;//自动拆箱:编译器会改为:int s = i5.intValue();
        double dd = dou;//double dd = dou.doubleValue();

    }

}
