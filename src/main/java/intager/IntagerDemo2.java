package intager;

/**
 * 包装类实用功能介绍
 */

public class IntagerDemo2 {
    public static void main(String[] args) {
        /*
            数字类型的包装类支持两个常量,MAX_VALUE,MIN_VALUE
            用于表示对应的基本类型的取值范围
         */
        int imax = Integer.MAX_VALUE;
        int imin = Integer.MIN_VALUE;
        System.out.println(imax+","+imin);
        double dmax = Integer.MAX_VALUE;
        double dmin = Integer.MIN_VALUE;
        System.out.println(dmax+","+dmin);

        /*
            包装类支持一个静态方法:parseXXX(String str)
            可以将字符串解析为对应的基本类型,前提是该字符串正确表达了基本类型可以保存的值
            否则会抛出异常:NumberFormatException

         */
        String str = "123";
        String str1 = "123.123";//不能转换为整数
        int d = Integer.parseInt(str);
        System.out.println(d);

        double dou  = Double.parseDouble(str);
        System.out.println(dou);

    }

}
