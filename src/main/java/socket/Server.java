package socket;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

/**
 * 聊天室服务端
 */
public class Server {
    /*
        java.net.ServerSocket 运行在服务端
        主要有两个作用:
        1.向系统申请服务端口(客户端就是通过这个端口与当前服务端程序建立链接的)
        2.监听服务端口,一旦一个客户端申请建立链接则接受该链接并返回一个Socket实例.
            通过这个Socket与客户端等链接进行双向交互
        我们可以把Socket想象为"电话",那么ServerSocket则相当于,某客服中心的"总机"
        总机下面链接者若干部电话,每个客户端打电话过来总机都会分配一个电话响并将其接起.
     */
    private ServerSocket serverSocket;

//    private PrintWriter[] allOut = {};
    private Collection <PrintWriter> allOut = new ArrayList();
    public Server(){
        try{
            /*
                实例化ServerSocket时需要指定服务端口,客户端通过这个端口与服务端建立
                链接,该端口不能和当前系统其它应用程序申请的端口重复,否则会抛出异常:
                java.net.BindException:address already in use
             */
            System.out.println("服务端正在启动...");
            serverSocket = new ServerSocket(8088);//0-65535之间
            System.out.println("启动完毕!");
        }catch(IOException e){
            e.printStackTrace();
        }

    }
    public void start(){
       try{
           while(true) {
               System.out.println("等待客户端连接...");
               Socket socket = serverSocket.accept();//阻塞
               System.out.println("一个客户端连接了!");
               //启动一个线程与该客户端交互
               ClientHandler handler = new ClientHandler(socket);
               Thread t = new Thread(handler);
               t.start();
           }

       }catch(IOException e){
           e.printStackTrace();
       }


    }

    public static void main(String[] args) {
        Server server = new Server();
        server.start();

    }
    /*
        该线程任务用于与指定的客户端进行交互
     */
    private  class  ClientHandler implements Runnable{
        private Socket socket;
        private String host;//记录客户端的地址信息

        public ClientHandler(Socket socket){
            this.socket = socket;
            //通过socket获取远端计算机地址信息的字符串格式
            host = socket.getInetAddress().getHostAddress();
        }
        public void run(){
                PrintWriter pw = null;
            try{
                /*
            Socket提供的方法:
            InputStream getInputStream()
            获取一个字节输入流,用于读取计算机发送过来的数据
            */
                InputStream in = socket.getInputStream();
                InputStreamReader isr = new InputStreamReader(in, "UTF-8");
                BufferedReader br = new BufferedReader(isr);

                //通过socket获取输出流,用于将消息发送给客户端
                OutputStream out = socket.getOutputStream();
                OutputStreamWriter osw = new OutputStreamWriter(out,"UTF-8");
                BufferedWriter bw = new BufferedWriter(osw);
                pw = new PrintWriter(bw,true);
                //将该客户端的输出流存入共享数组allOut中,供其他ClientHandler转发信息
//                    synchronized (this){//不行,不同线程看到的ClientHandler,
//                    synchronized (this){//不行,因为会对数组扩容(相当于创建了新的数组对象)
                synchronized (allOut){//对于集合而言,直接锁集合对象即可

                    allOut.add(pw);
//                    allOut = Arrays.copyOf(allOut,allOut.length+1);

//                    allOut[allOut.length - 1] = pw;
                }
                sendMessage(host+"上线了"+allOut.size());
//                sendMessage(host+"上线了,当前在线人数:"+allOut.length);
                String message;
           /*
            这里使用缓冲字符流的readLine方法读取一行来自客户端发送过来的字符串
            该方法调用后会出出现"阻塞",知道客户端确实发送了一行字符串该方法才会将
            这行字符串返回.
            如果返回值为null,则说明客户端断开链接(客户端调用socket.close()
            正常断开链接).
            如果客户端强行中断程序(没有调用socket.close())则这里会抛出异常
            java.net.SocketException: Connection reset
            */
                while ((message = br.readLine()) != null) {
                    System.out.println(host+"说:" + message);
                    //将消息大宋给所有客户端(遍历allOut数组)
                    sendMessage(host+"说:"+message);
                }
            }catch(IOException e){
                e.printStackTrace();
            }finally{
                synchronized (allOut){
                    allOut.remove(pw);
                }
                System.out.println(host+"下线了,当前在线人数"+allOut.size());
                try{
                    socket.close();
                }catch(IOException e){
                    e.printStackTrace();
                }

            }
        }
        public void sendMessage(String message){
            synchronized (allOut){
                for(PrintWriter pw:allOut){
                    pw.println(message);
                }
//                for(int i = 0;i<allOut.length;i++){
//                    allOut[i].println(message);
//                }
            }
        }
    }
}
