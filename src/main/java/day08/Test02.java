package day08;
/**
 * 修改下面代码的编译
 * @author Xiloer
 *
 */
public class Test02 {
	public static void main(String[] args) {
		Thread t1 = new Thread(()-> Foo.dosome());
		Thread t2 = new Thread(()-> Foo.dosome());

		t1.start();
		t2.start();
	}
}

class Foo {
    public static void dosome() {
        synchronized (Foo.class) {
            System.out.println("hello!");
        }
    }
}

