package doc;

/**
 * 文档注释
 * 文档注释只能在三个地方使用:类上,常量上,方法上
 * 文档注释可以被idea读取作为提示使用,但实际上最终用途是通过javadoc命令生成手册.
 * @author maxiangyu
 */
public class APIDocDemo {
    /**
     * 为给定的用户添加一个问候语,"你好"xxx
     * @param name 指定的用户名
     * @return 含有问候语的字符串
     */
    public String sayHello(String name){
        return "你好"+name;
    }
}
