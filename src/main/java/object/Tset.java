package object;

/**
 * 用于测试常用的Object中的定义方法
 * toString与equals
 */

public class Tset {
    public static void main(String[] args) {
        Point  p = new Point(1,2);
        Point p2 = new Point(1,2);
        /*
            Point 是引用类型,因此p与p2变量保存的是对象的地址,由于p和p2指向两个
            不同的对象,所以地址不相同
         */
        System.out.println(p==p2);
        /*
            Object提供了方法:
            boolean equals(Object 0)
            用来比较两个对象的内容是否相同,若当前当前对象与
            对象o对象的内容相同同时返回true
            注意:该方法若使用就应当被派生类重写,因为Object中的实现就是用"=="比较的
            不重写则并非真正的比较
         */
        boolean equals = p.equals(p2);//Point若不重写,则返回值为false
        System.out.println(equals);
        /*
            Object中第一个常被使用的方法为
            String toString()
            该方法的作用是将当前对象转换为一个字符串
            Object默认实现返回的字符串格式:
            包名.类名@地址信息

            开发中我们经常使用System.out.println(Object o)方法将一个对象输出到
            控制台，而该方法就是调用传入的对象的toString方法现将其转换为一个字符串后
            再输出到控制台。因此如果输出的对象没有重写toString方法，则输出的就是地址
            信息，对于开发中的帮助不大。

         */
        System.out.println(p);////默认输出:object.Point@1b6d3586
        String line = "这个对象内容为:"+p;
        System.out.println(line);
    }
}
