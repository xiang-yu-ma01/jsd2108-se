package collection;

import java.util.ArrayList;
import java.util.Collection;

/**
 * 集合
 * java.util.Collection接口
 *
 * 集合可以保存一组元素,并且存储元素等操作都以方法的形式提供出来,使用方便.
 * Collection是一个接口,规定了集合都应当具备的相关功能,下面有众多实现类,实现了不同种类
 * 的数据结构,各有各的特点,
 * 比较常见的两类集合:
 * java.util.list接口:继承自Collection,这类集合的特点是有序,并且可以存放重复元素
 * java.util.Set接口:继承自Collection,这类集合不能存放重复元素
 * 元素是否重复的判定依据是元素自身的equals比较
 *
 */
public class CollectionDemo {
    public static void main(String[] args) {
        Collection c = new ArrayList();
    /*
        boolean add(E e)
        向前当前集合添加一个元素,元素添加成功后返回true
        注意,集合只能存放引用类型元素,存放基本类型数据时会自动装箱为包装类实例存放
     */
        c.add("one");
        c.add("two");
        c.add("three");
        c.add("four");
        c.add("five");
        System.out.println(c);
        /*
            int size()
            返回当前集合的元素个数
         */
        int size = c.size();
        System.out.println("size:"+size);
        /*
            boolean isEmpty()
            判断当前集合是否为空集
            当且仅当集合的size为0时,返回值为true
         */
        boolean isEmpty = c.isEmpty();
        System.out.println("是否为空集:"+isEmpty);

        //清空集合
        c.clear();
        System.out.println("size:"+c.size());
        System.out.println("是否为空集:"+c.isEmpty());
        System.out.println(c);

    }

}
