package collection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * java.util.List接口 通常称为线性表
 * List集继承Collection.特点是:可以存放重复元素并且有序
 *
 * 常用实现类:
 * java.utilArrayList:内部使用数组实现,查询性能好
 * java.util.LinkedList:内部使用链表实现.增删性能更好,收尾增删元素性能更佳
 * 在对集合操作性能没有特别苛刻的要求下通常首选为ArrayList
 *
 */
public class ListDemo {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
//      List<String> list = new ListedList<>();

        list.add("one");
        list.add("two");
        list.add("three");
        list.add("four");
        list.add("five");
        System.out.println(list);
        /*
            E get(int index)
            获取给定下标对应的元素
         */
        //获取第三个元素
        String str = list.get(2);
        System.out.println(str);

        for(int i = 0;i<list.size();i++){
            str = list.get(i);
            System.out.println(str);
        }
        /*
            E set(int index,E e)
            将给定元素设置到指定位置,返回值为被替换的元素
         */
        //[one,six,three,four,four]
        //String old = list.set(1,"six");
        //System.out.println(old);
        System.out.println(list);

        //反转集合
        /*String old= list.set(list.size()-1,list.get(0));
        String old1= list.set(0,old);
        String old3= list.set(list.size()-2,list.get(1));
        String old4= list.set(1,old3);
        System.out.println(list);

         */

        for(int i = 0;i<list.size()/2;i++){
//            String e = list.get(i);//获取整数位置上的元素
//            e = list.set(list.size()-1-i,e);//将正数位置上的元素存入倒数位置
//            list.set(i,e);
            list.set(i,list.set(list.size()-1-i,list.get(i)));
        }
        System.out.println(list);

        Collections.reverse(list);//反转集合
        System.out.println(list);



    }
}
