package collection;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * 集合的遍历
 *
 * Iterator iterator()
 * 该方法会获取一个用于遍历当前集合元素的迭代器
 *
 * java.util.Iterator接口,迭代器接口.规定了迭代器遍历集合的相关操作.使用迭代器遍历
 * 集合遵循的过程为:问,取,删的步骤重复进行即可.其中删除元素不是遍历过程中的必要操作.
 *
 * 不同的集合实现类都提供了一个可以遍历自身元素的迭代器实现类,我们无需记住他们的类名,仅以
 * 多态的角度当他是Iterator看待并操作即可.
 */
public class IteratorDemo {
    public static void main(String[] args) {
        Collection c = new ArrayList();
        c.add("one");
        c.add("#");
        c.add("two");
        c.add("#");
        c.add("three");
        c.add("#");
        c.add("four");
        c.add("#");
        c.add("five");
        System.out.println(c);
        //通过集合获取迭代器
        Iterator it = c.iterator();

        //询问迭代器是否还有下一个元素
        while(it.hasNext()){
            String str = (String)it.next();//取下一个元素(刚通过hasNext询问)
            if("#".equals(str)){
                /*
                    迭代器要求遍历的过程中不得通过集合的方法增删元素,否则会抛出
                    异常:java.util.ConcurrentModificationException
                 */
//                c.remove(str);
                /*
                    迭代器提供了remove方法,可以删除本次通过next方法遍历出来的元素
                 */
                it.remove();
            }
            System.out.println(str);
        }
        System.out.println(c);
    }
}
