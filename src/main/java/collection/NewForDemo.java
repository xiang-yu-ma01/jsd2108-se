package collection;


import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * JDK5推出时推出了两个和集合息息相关的特性:增强型for循环和泛型
 *
 * 增强型for循环,也称为新循环.它可以让我们用更精简的语法遍历集合和数组.该特性可以使得
 * 我们遍历集合或者数组的语法相同,但是他并非真的语法(JVM不认可),而是编译器认可的,编译器
 * 最终会将新循环改为传统循环的遍历集合(迭代器)或者数组(普通的for循环)
 */
public class NewForDemo {
    public static void main(String[] args) {
       String[] array ={"one","two","three","four","five"};
       for(int i=0;i<array.length;i++){
           String str = array[i];
           System.out.println(str);
       }
       //编译器会将下面的代码改为上面的代码
       for(String str:array){
           System.out.println(str);
       }
        //泛型,也称为参数化类型.
//  泛型可以允许我们使用一个类时指定该类中的某个属性的类型,方法参数类型或
//  返回值类型
 // 泛型用用最多的地方就是集合中,用于指定集合中的元素类型
       Collection<String> c = new ArrayList<>();
       c.add("1");
       c.add("2");
       c.add("3");
       c.add("4");
       c.add("5");
       System.out.println(c);
       for(String str:c){
           System.out.println(str);
       }
        //上面的新循环会被编译器改为下面这种迭代器方式遍历集合
       Iterator<String> it = c.iterator();//迭代器泛型必须指定的集合一致
       while(it.hasNext()){
           String str = it.next();//获取元素时无需再造型
           System.out.println(str);
       }
    }
}
