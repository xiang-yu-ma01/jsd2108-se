package collection;

import java.util.ArrayList;
import java.util.Collection;

/**
 * 集合的很多操作都受到元素的方法影响,其中影响最多的就是equals方法
 */
public class CollectionDemo2 {
    public static void main(String[] args) {
        Collection c = new ArrayList();
//          Collection c = new HashSet();//不能存放重复元素的的集合
        c.add(new Point(1,2));
        c.add(new Point(1,2));
        c.add(new Point(3,4));
        c.add(new Point(5,6));
        c.add(new Point(7,8));
        c.add(new Point(9,0));
        /*
            集合重写了toString,输出的格式
            [元素1.toString(),元素2.toString(),元素3.toString(),...]
            因此元素的toString方法会影响集合toString展现的内容
         */

        System.out.println(c);

        Point p = new Point(1,2);
        /*
            boolean contains(Object o)
            判断当前集合是否包含给定元素
            集合判断元素是否被包含是依据该元素是否与集合现有的元素存在equals比较为true
            情况,存在则认为包含
         */
        boolean contains = c.contains(p);
        System.out.println("contains:"+contains);

        c.remove(p);
        System.out.println(c);



    }
}
