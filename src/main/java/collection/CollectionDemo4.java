package collection;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

/**
 * 集合间的操作
 */
public class CollectionDemo4 {
    public static void main(String[] args) {
        Collection c1 = new ArrayList();
        c1.add("c++");
        c1.add("java");
        c1.add("c#");
        System.out.println("c1:"+c1);

        Collection c2 = new HashSet();
        c2.add("ios");
        c2.add("android");
        c2.add("java");
        System.out.println("c2:"+c2);
        /*
            boolean addAll(Collection e)
            将给定集合的所有元素添加到当前集合中(并取并集).执行后当前集合元素发生变化
            则返回true
         */
        c2.addAll(c1);
        System.out.println("c1:"+c1);
        System.out.println("c2:"+c2);

        Collection c3 = new ArrayList();
        c3.add("java");
        c3.add("ios");
        System.out.println("c3:"+c3);
        /*
            boolean containsAll(Collection c)
            判断当前集合是否包含给定集中的所有元素
         */
        boolean contains = c2.containsAll(c3);
        //下面的语句是判断c2中是否有一个元素是c3集合本身;
//        boolean contains = new c2.contains(c3);
        System.out.println("全包含:"+contains);
        /*
            删除当前集合中给定集合中的共有元素
         */
        c2.removeAll(c3);
        System.out.println("c2:"+c2);
        System.out.println("c3:"+c3);

    }
}
