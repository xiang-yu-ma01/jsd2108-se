package io;

import java.io.*;

/**
 * 自行组建流链接，使用PrintWriter按行写出字符串
 */

public class PWDemo2 {
    public static void main(String[] args) throws FileNotFoundException, UnsupportedEncodingException {

        FileOutputStream fos = new FileOutputStream("pe.txt");
        OutputStreamWriter osw = new OutputStreamWriter(fos,"UTF-8");
        BufferedWriter bw = new BufferedWriter(osw);
        PrintWriter pw = new PrintWriter(bw);

        pw.println("啊！牡丹~~~~~~·");
    pw.println("百花丛中最鲜艳！");
        System.out.println("完毕！");
        pw.close();


    }
}

