package io;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * 文件流的追加写模式
 */
public class FileOutputStreamDemo2 {
    public static void main(String[] args) throws IOException {
        /*
            构造方法如果为:
            FileOutputStream(File file)
            FileInputStream(File fileName)
            则该文件流为覆盖模式,即:
            创建文件流时若指定的文件已经存在了,则会将该文件内容先清空,之后在顺序的
            写入新数据

            FileOutputStream(File )
            FileInputStream(File )
            如果创建时传入第二个参数,且值为true时,则文件流为追加模式,即:
            若文件已存在,值为原数据全部保留,新写入的内容都会顺序向后追加
         */
        FileOutputStream fos = new FileOutputStream("./fos.txt");
        String line = "铠甲合体";
        fos.write(line.getBytes(StandardCharsets.UTF_8));
        fos.write("炎龙铠甲".getBytes(StandardCharsets.UTF_8));
        System.out.println("穿风刺");
        fos.close();



    }


}
