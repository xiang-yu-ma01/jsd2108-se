package io;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * java将流分为两类:
 * 节点流与处理流
 *
 * 节点流:也称为低级流,是实际链接程序与另一端的"管道",负责实际读写数据的流,
 *      读写一定是建立在节点流基础上进行的
 * 处理流:也称为高级流,不能独立存在,必须链接在其他流上,目的是当数据流经流时对数据
 *      做某种加工处理,简化我们对应的操作.
 *
 * 实际开发中我们经常串联一组高级流并最终链接到,讴歌低级流上,读写数据的过程中以流水线式
 * 的对数据做加工处理,这也称为"流的链接"
 *
 * 缓冲流:java.io.BufferOutputStream和BufferedInputStream
 *
 * 缓冲流是一对高级流,作用是提高读写效率
 *
 * 缓冲流内部维护一个字节数组,默认大小为8k,无论我们使用缓冲流时是单字节还是块读写
 * 最终都会被缓冲流转换为块读写8k一次的顺序读写来保证读写效率
 * */
public class CopyDemo3 {
    public static void main(String[] args) throws IOException {
        FileInputStream fis = new FileInputStream("./Git.exe");
        BufferedInputStream bis = new BufferedInputStream(fis);
        int d;
        long start = System.currentTimeMillis();
        System.out.println(start);
        while((d = bis.read())!=1){

        }
    }

}
