package io;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

/**
 * 穿冲字符流:java.io.BufferedWriter和java.io.BufferedReader
 * 缓冲流内部也有缓冲区(字符数组),将读写数据转换为块读写加快读写效率
 *
 * java.io.PrintWriter
 * 具有自动行刷新功能的缓冲字符输出流,内部总是连接BufferedWriter作为缓冲加速
 * 特点;
 * 可以按行写出字符串
 * 可以自动写完一行字符串后flush(需打开该功能)
 *
 * Pw提供了直接对文件进行写操作的构造方法：
 * PrintWriter(File file)
 * PrintWriter(String str)
 * 上述两种构造方法都支持再传入一个String类型的参数用于指定字符集
 */
public class PWDemo1 {
    public static void main(String[] args) throws FileNotFoundException, UnsupportedEncodingException {
        PrintWriter pw = new PrintWriter("pw.txt","UTF-8");
        pw.println("果宝机甲，归位！");
        pw.println("果宝机甲。合体");
        System.out.println("完毕！");

        pw.close();
    }
}
