package io;

import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * 缓冲输出流写出数据的缓冲区问题
 */
public class FlushDemo {
    public static void main(String[] args) throws IOException {
        FileOutputStream fos = new FileOutputStream("./bos.txt");
        BufferedOutputStream bos = new BufferedOutputStream(fos);
        String line = "铠甲合体!";
        bos.write(line.getBytes(StandardCharsets.UTF_8));
        /*
            void flush()
            强制将缓冲区(8k数组)中已经缓存的数据一次性写出
         */
        bos.flush();

        System.out.println("写出完毕");
        //缓冲输出流的close方法中会调用一次flush确保所有数据写出,
        bos.close();

    }
}
