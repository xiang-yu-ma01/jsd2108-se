package io;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * 写出文本数据
 */
public class WriteStringDemo {
    public static void main(String[] args) throws IOException {
        FileOutputStream fos = new FileOutputStream("./fos.txt");
        String line = "炎龙铠甲合体,炎龙斩!";
        byte[] data = line.getBytes(StandardCharsets.UTF_8);
        fos.write(data);

        System.out.println("变身完毕!");
        fos.close();
    }
}
