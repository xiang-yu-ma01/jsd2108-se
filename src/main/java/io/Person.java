package io;

import java.io.Serializable;
import java.util.Arrays;

/**
 * 使用当前类测试对象的序列化与反序列化
 */
public class Person implements Serializable {
    /*
        实现序列化接口后,建议制定一个常量,serialVersionUID
        这个常量用于制定当前类的版本号,不指定时,对象在序列化时对象流会结合该类的结构生成
        一个版本号,只不过当前类只要发生过改变,版本号就会跟着改变
        版本号影响对象反序列化是否成功:
        当对象输入流在进行反序列化时发现该对象与当前类的版本号不一致时会抛出异常:
        java.io.InvalidClassException
        如果我们自行制定了版本号,那么无论当前类结构怎么修改,只要版本号不变
        那么反序列化之前的对象就可以成功
     */
    public static final long serialVersionUID = 1L;//版本号
    private String name;
    private int age;
    private String gender;
    private String nickname;
    /*
        当某个属性被关键字transient修饰后,那么这个对象在序列化时该属性会被忽略
        忽略必要的属性可以达到序列化对象的"瘦身"效果,减少不必要的开销.
     */
    private transient String[] otherInfo;

    public Person(String name, int age, String gender, String[] otherInfo) {
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.otherInfo = otherInfo;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", gender='" + gender + '\'' +
                ", otherInfo=" + Arrays.toString(otherInfo) +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String[] getOtherInfo() {
        return otherInfo;
    }

    public void setOtherInfo(String[] otherInfo) {
        this.otherInfo = otherInfo;
    }
}
