package io;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * 使用字符流读取文本数据
 */
public class ISRDemo {
    public static void main(String[] args) throws IOException {
        //读取osw.txt文本中的信息
        FileInputStream fis = new FileInputStream("osw.txt");

        /*byte[] data = new byte[3];
        fis.read(data);
        String str = new String(data,"UTF-8");
        System.out.println(str);*/
        /*
            上述代码并不能完全做到读取文件中的第一个汉字,因为中文的UTF-8编码每个字是
            3个字节,但是英文则仅占1个字节,所以如果文件中第一个字是英文则会出现问题.
         */
        //创建一个足够大的字节数组
 //       byte[] data = new byte[200];
  //      int len = fis.read(data);
 //       System.out.println(len);
 //       String str = new String(data,0,len,"UTF-8");
  //      System.out.println(str);

        //
        InputStreamReader isr = new InputStreamReader(fis,"UTF-8");

       /* int d = isr.read();
        System.out.println((char)d);*/
        int d ;
        while((d=isr.read())!=-1){
            System.out.print((char)d);
        }


        fis.close();
    }
}
