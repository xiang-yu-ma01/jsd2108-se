package io;

import java.io.*;

/**
 * 缓冲字符输入流:java.io.BufferedReader
 * 块读文本数据加速,并且可以按行读取字符串
 */
public class BRDemo {
    public static void main(String[] args) throws IOException {
        FileInputStream fis = new FileInputStream("./src/main/java/io/BRDemo.java");
        InputStreamReader isr = new InputStreamReader(fis);
        BufferedReader br = new BufferedReader(isr);
        /*
            String readLine()
            读取一行字符串,一行结束的标志为换行符.但是返回的这一行字符串不包含最后
            的换行符
            如果返回值为null,则表示流读取到了末尾.
         */
        String line;
        while ((line = br.readLine()) != null) {
            System.out.println(line);
        }
        br.close();
    }
}

