package io;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * java IO
 * IO:input和output 即:输入与输出
 * java将读写按照方向划分为输入和输出，其中输入负责读，输出负责写。
 * java将读写操作的API比喻为"流"。流:向着同一侧顺序移动的过程。在java中读写字节的API
 * 称为字节流。对应的两个超类为:
 * java.io.InputStream和java.io.OutputStream，他们都是抽象类，规定了读写方法。
 * InputStream是所有字节输入流的超类，用于读取字节。本身不能实例化，下面有众多实现类。
 * 例如:文件输入流FileInputStream,用于读取文件数据
 * OutputStream是所有字节输出流的超类，用于写出字节。本身不能实例化，也有众多实现类。
 *
 * 文件流:java.io.FileInputStream和FileOutputStream
 * 是实际链接程序与文件的"管道"，负责读写文件的两个流。
 *
 *
 */
public class FileOutputStreamDemo {
    public static void main(String[] args) throws IOException {
        /*
            向当前目录下的fos.dat文件中写入数据
            文件输出流常见的构造方法:
            FileOutputStream(File file)
            FileOutputStream(String fileName)
            文件流在创建时若发现指定的文件不存在时会将该文件创建出来,如果指定的文件
            所在的目录不存在则会出现抛出异常FileNotFoundException
         */
        //File file = new File("./fos.dat");
        //FileOutputStream fos = new FileOutputStream(file);
        FileOutputStream fos = new FileOutputStream("./fos.dat");
        /*
            OutputStream字节输出流提供了写出字节的方法:
            void write(int d)
            写出1个字节。将给定的int值对应的2进制的"低八位"写出

            文件流输出流继承自OutputStream，所以该方法是将给定内容写入文件。

            fos.write(1)
            int型的1对应的2进制:
                                       vvvvvvvv 写的是这8位
            00000000 00000000 00000000 00000001

            fos.dat文件中内容为:
            00000001
         */
        fos.write( 1);
        /*
            fos.write(2)
            int型的2对应的2进制:
                                       vvvvvvvv 写的是这8位
            00000000 00000000 00000000 00000010

            fos.dat文件中内容为:
            00000001 00000010
         */
        fos.write(2);
        System.out.println("写出完毕!");
        fos.close();

    }

}
