package io;

import java.io.*;
import java.util.Scanner;

/**
 * 完成建议记事本
 *
 * 程序启动要求用户先输入一个文件名，之后在输入的内容全部按行写入这个文件中
 * 当用户单独输入exit时程序退出
 *
 * 要求：创建    PrintWriter时自行完成流链接
 */
public class Tset02 {
    public static void main(String[] args) throws UnsupportedEncodingException, FileNotFoundException {
        Scanner sca = new Scanner(System.in);
        FileOutputStream fos = new FileOutputStream("fileName");
        OutputStreamWriter osw = new OutputStreamWriter(fos,"UTF-8");
        BufferedWriter bw = new BufferedWriter(osw);
        /*
            创建 PrintWriter 时如果第一个参数为一个流,那么这个构造方法就支持再传一个
            boolean型参数,作用是指定是否打开自动刷新功能
            如果打开了自动行刷新功能,那么每当使用println方法写出一行字符串时就会自动flush
         */
        PrintWriter pw = new PrintWriter(bw,true);
        System.out.println("（单独输入 exit退出）你想在文本里干什么：");
        while(true){
            String line = sca.nextLine();
            if("exit".equals(line)){
                break;
            }
            pw.println(line);
        }
        pw.close();
        System.out.println("撒由那拉");

    }
}
