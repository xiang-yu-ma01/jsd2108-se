package io;

import java.io.*;

/**
 * java将流按照读写单位划分为:字节流和字符流
 * java.ioInputStream和java.io.OutputStream是所有字节输入流与输出流的超类
 * java.io.Read和java.io.Writer则是所有字符输入输出流的超类
 * 字节流的超类与字符流的超类是平级关系,互相没有继承关系
 *
 * 字符流底层本质还是读写文字(计算机底层都是二进制的字节).只不过字符与字节的转换由字符流
 * 完成,使得我们读写文本数据变得很方便,也需要注意:字符流仅适合读写文本数据!!!
 *
 * 转换流:InputStreamReader和OutputStreamWriter
 * 常用的字符流实现类,实际开发中我们不会直接操作它们,但是在流链接中它们是中药的一环
 *
 * 实际开发中我们常用的其他的高级字符流都有一个共同特点,在流链接中它们只链接其他的字符流,
 * 而通常我们使用的低级流都是字节流,这导致流链接中无法将他们直接串联起来,此时转换流的
 * 作用就发挥于此了,转换流本身是字符流,而他可以链接字节流,其他的字符流又可以链接上他
 * 使得他在流链接中起到了""转换器"的作用.
 */
public class OSWDemo {
    public static void main(String[] args) throws IOException {
        //想文本osw.txt中写入文本数据
        FileOutputStream fos = new FileOutputStream("osw.txt");
        OutputStreamWriter osw = new OutputStreamWriter(fos,"UTF-8");
        /*
            字符流支持直接写出字符串的write方法
         */
        osw.write("迪迦奥特曼,变身!");
        osw.write("赛文!赛文!赛文!");
        System.out.println("写出完毕");
        /*
            转换流内部有一小端缓冲区(字节数组),因此写出后注意flush或者最后的clpse,否则可能
            出现数据没有写出的情况.
         */

        osw.close();

    }
}
