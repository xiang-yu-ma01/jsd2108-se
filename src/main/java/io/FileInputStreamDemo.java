package io;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * 文件输入流,用于从文件中读取字节
 */

public class FileInputStreamDemo {
    public static void main(String[] args) throws IOException {
        FileInputStream fis = new FileInputStream("./fos.dat");
        /*
            InputStream中定义了读取1个字节的方法:
            int read()
            读取1个字节并以int型返回。返回的int值对应的2进制低八位有效
            如果返回的int值为整数-1，则表示流读取到了末尾。

            FileInputStream实现了该方法，用于从文件中读取1个字节
         */

        /*
            fos.dat文件数据:
            00000001 00000010
            ^^^^^^^^读取第一个字节
            int d = fis.read()执行后
            前面24位2进制补"0",将读取到的1字节的8位2进制放在最后八位上
                                         vvvvvvvv
            d:00000000 00000000 00000000 00000001
         */
        int d = fis.read();
        System.out.println(d);//1
        /*
            fos.dat文件数据:
            00000001 00000010
                     ^^^^^^^^读取第二个字节
            d = fis.read()执行后
            前面24位2进制补"0",将读取到的1字节的8位2进制放在最后八位上
                                         vvvvvvvv
            d:00000000 00000000 00000000 00000010
         */
        d = fis.read();
        System.out.println(d);//2

        /*
            fos.dat文件数据:
            00000001 00000010 文件末尾
                              ^^^^^^^^读取到文件末尾了
            d = fis.read()执行后
            读取到文件末尾时，返回的int值为整数-1对应的2进制。
            d:11111111 11111111 11111111 11111111
         */
        d = fis.read();
        System.out.println(d);//-1

        fis.close();

    }
}
