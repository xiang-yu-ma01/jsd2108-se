package io;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

/**
 * 实现简易的记事本工具
 * 程序执行后,将用户在控制台输入的每一行字符串都写入文件note.txt中
 * 当用户单独输入exit时程序退出
 *
 * 写入文件的字符串编码要求为UTF_8,不要考虑换行问题
 */
public class DataTest {
    public static void main(String[] args) throws IOException {

        FileOutputStream fos = new FileOutputStream("./note.txt");

        Scanner sca = new Scanner(System.in);
        System.out.println("(单独输入exit 时退出)在文本中输入你的文字:");
        while(true){
            String d = sca.nextLine();
            if("exit".equals(d)){
                break;
            }
            byte[] data = d.getBytes(StandardCharsets.UTF_8);
            fos.write(data);
        }
        System.out.println("帝皇铠甲!合体");
        fos.close();



    }
}
