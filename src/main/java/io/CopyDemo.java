package io;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class CopyDemo {
    public static void main(String[] args) throws IOException {
        FileInputStream fis = new FileInputStream("./psc.jpg");
        FileOutputStream fos = new FileOutputStream("./psc_cp.jpg");
        int d;
        long start = System.currentTimeMillis();
        System.out.println(start);
        while((d=fis.read())!=-1){//读取到文件末尾时就不许要再循环了
           fos.write(d);
        }
        long end = System.currentTimeMillis();
        System.out.println("复制完毕!耗时:"+(end-start)+"ms");
        fis.close();
        fos.close();

    }
}
