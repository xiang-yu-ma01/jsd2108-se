package io;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

/**
 * 对象流:java.io.ObjectInOutputStream和ObjectOutputStream
 * 对象流是一堆高级流,作用是进行对象序列化与反序列化
 * 对象序列化:将一个java对象转换为一组字节的过程
 * 反序列化:将一组字节(对象序列化后的字节)还原为java对象的过程
 */
public class OjectOutputStreamDemo {
    public static void main(String[] args) throws IOException {
        String name = "大古";
        int age = 23;
        String gender = "男";
        String[] otherInfo={"是个胜利队员","来自日本","出场较少","会变身迪迦","广大男生的梦想"};

        Person p = new Person(name,age,gender,otherInfo);
        System.out.println(p);
        FileOutputStream fos = new FileOutputStream("person.obj");
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        /*
            对象输出流在写出的时候,要求写出的对象所属的类必须实现可序列化接口,
            否则会抛出异常:java.ioNoSerializableExcetion

            可序列化接口:java.io.Serializable
         */
        oos.writeObject(p);
        System.out.println("变身完毕!");
        oos.close();

    }
}
