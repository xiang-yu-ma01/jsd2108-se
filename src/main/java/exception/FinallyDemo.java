package exception;

/**
 * finally块
 * finally是异常处理机制的最后一块,他可以直接跟在try之后或者最后一个
 * catch之后,finally可以保证只要程序执行到try语句块中,无论try块中是否
 * 抛出异常,finally都必定会执行.
 * 因此我们通常将释放资源一类操作放在finally中确保执行,例如IO操作完毕后的关闭动作.
 *
 */
public class FinallyDemo {
    public static void main(String[] args) {
        System.out.println("程序开始了");
        try{
            String str = "null";
            System.out.println(str.length());
            //return 依然是要将finally执行后方法才会返回.
            return;
        }catch(Exception e){
            System.out.println("有错");
        }finally{
            System.out.println("finally语句块执行了");
        }
        System.out.println("程序结束了");

    }

}
