package exception;

/**
 * 异常常见的方法
 */
public class ExceptionApiDemo {
    public static void main(String[] args) {
        try {
            String str = "abc";
            System.out.println(Integer.parseInt(str));
        }catch(Exception e){
            //向控制台输出错误堆栈信息
            e.printStackTrace();
            //获取错误信息,可以用来记录日志时使用或者提示给用户时使用
            String message = e.getMessage();
            System.out.println(message);
        }
    }
}
