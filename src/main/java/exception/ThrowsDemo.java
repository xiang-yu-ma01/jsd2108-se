package exception;

import java.awt.*;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * 当子类重写超类含有throws声明异常抛出的方法时对throws的重写规则
 */
public class ThrowsDemo {
    public void dosome()throws IOException, AWTException {}
}
class SubClass extends ThrowsDemo{
    //public void dosome()throws IOException, AWTException{}
    //重写时可以不再抛出任何异常
   // public void dosome(){}
    //重写时可以仅抛出部分异常
    //public void dosome()throews IOException{}
    //重写时可以抛出超类方法抛出异常的子类异常
    public void dosome()throws FileNotFoundException {}

    //public void dosome()throws SQLException{}
    //不允许抛出额外的异常(超类中没有的且没有继承关系的)
    //public void dosome()throws Exception{}
    //不允许抛出超类方法抛出异常的超类型异常
}