package exception;

/**
 * 非法的年龄异常
 *
 * 自定义异常
 * java允许我们自行定义新的异常类型,通常用于表达满足语法而不满足某些业务场景的错误问题
 * 自定义异常需要注意以下几点:
 * 1.异常的类名要见名知意
 * 2.必须是Exception的子类型
 * 3.提供超类型异常提供的所有种类构造器
 */
public class IllagalAgeException extends Exception{
    public IllagalAgeException() {
    }

    public IllagalAgeException(String message) {
        super(message);
    }

    public IllagalAgeException(String message, Throwable cause) {
        super(message, cause);
    }

    public IllagalAgeException(Throwable cause) {
        super(cause);
    }

    public IllagalAgeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}


