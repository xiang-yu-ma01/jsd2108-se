package exception;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * JDK7推出的异常处理机制中的自动关闭特性
 *
 * 旨在异常处理机制中的finally中像关闭流这样的操作得以简化
 */
public class AutoCloseableDemo {
    public static void main(String[] args) {
        //该特性是编译器认可的,在try()中定义的流最终会被
        //编译器改为在finally中关闭
        //改后的代码可参考FinallyDemo2案例的样子
        try (
                //只有实现了AutoCloseable接口的类才可以在这里定义并初始化
                FileOutputStream fos = new FileOutputStream("fos.dat")
        ) {
            fos.write(1);
        } catch (IOException e) {
            e.printStackTrace();

        }
    }
}

