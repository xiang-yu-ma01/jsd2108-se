package exception;

/**
 * finally常见面试题
 * 1.请简述:final,finally,finalize
 * finalize是一个方法,被定义在Object中,这意味着java中所有的类都具有该方法
 * 这个方法是GC调用的方法,当一个对象即将被GC释放时,GC会调用该方法,调用完毕就释放对象
 * 所以该方法是一个对象生命周期中的最后一个方法
 * Object中该方法里没有任何操作,如需要可自行重写,该方法不能有耗时的操作,会影响GC工作.
 */
public class FinallyDemo3 {
    public static void main(String[] args) {
        System.out.println(
                test("0")+","+test(null)+","+test("")
        );//3,3,3

        }
    public static int test(String str) {
        try {
            return str.charAt(0) - '0';
        } catch (NullPointerException e) {
            return 1;
        } catch (Exception e) {
            return 2;
        } finally {
            return 3;
        }
    }



}
