package exception;

/**
 * 使用当前类测试异常的抛出
 */
public class Person {
    private int age;

    public int getAge() {
        return age;
    }

    public void setAge(int age) throws IllagalAgeException {
        if(age<0 || age>=120){
            //throw new RuntimeException("年龄不合法");
            /*
                当我们使用throw显示对外抛出一个异常时,除了RuntimeException这类
                异常之外的所有异常编译器都要求必须在方法上使用throws声明该异常
                的抛出来通知使用者解决该异常.
             */
            throw new IllagalAgeException("年龄不合法");
            
        }
        this.age = age;
    }
}
