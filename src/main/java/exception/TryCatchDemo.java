package exception;

import java.util.Arrays;

/**
 * 异常处理机制中的try-catch
 * 语法:
 * try{
 *     可能出现异常的代码块片段
 *
 * }catch(XXXException e){
 *     try中出现的XXXException后出的处理手段
 * }
 */
public class TryCatchDemo {
    public static void main(String[] args) {
        System.out.println("程序开始了");
        /*
            异常处理机制是关心那些程序运行时明知道会发生,但是又无法通过逻辑完全避免
            的问题,这时我们通过异常处理机制事先写好一旦发生该情况时的解决方法,避免程序中断
            能通过逻辑避免的问题都属于bug!不应当用异常处理机制来解决,比如空指针异常
         */
        //JVM执行到某句话时出现了异常则会实例化该异常并将其抛出
        //String str = null;
        //String str = "";
        String str = "a";
        try {
            System.out.println(str.length());//抛出一个空指针异常实例
            System.out.println(str.charAt(0));
            System.out.println(Integer.parseInt(str));
            System.out.println("这句话不会执行了SB!");
        }/*catch(NullPointerException e){
            System.out.println("出现了空指针并解决了!");
            //catch可以定义多个,当try中出现多个无法避免的异常时,可以分别捕获并处理
        }*/catch(NullPointerException | StringIndexOutOfBoundsException e){
            System.out.println("出现了空指针并解决了!或程序结束了");
            //可以在最后一个catch处捕获Exception,避免因为意想不到
            // 的异常抛出导致的程序中断
        }catch(Exception e){
            System.out.println("反正就是个错!");
        }
        System.out.println("程序结束了");
    }
}
