package exception;

/**
 * throw关键字用于主动对外抛出一个异常,通常下列情况我们会主动抛出异常:
 * 1.当前代码片段出现了异常,但是该异常并应当在当前代码片段中被解决时可以对外抛出
 * 2.当遇到满足语法,但是不满足业务问题时,可以主动抛出一个异常通知调用者不用当这样操作
 *
 * 本案例是基于第二种情况测试异常的抛出
 */
public class ThrowDemo {
    public static void main(String[] args) {
        Person p = new Person();
        try {
            /*
                当我们调用一个含有throws声明异常抛出方法时,编译器要求我们必须
                处理这个异常处理方式有两种:
                1.使用try-catch自行捕获并处理
                2.在当前方法上继续使用throws声明将该异常继续对外抛出.
             */
            p.setAge(1000);//满足语法不满足业务
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("此人年龄为:"+p.getAge());
    }
}
