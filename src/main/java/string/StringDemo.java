package string;

/**
 * String类的每一个实例用于表示一组字符
 * String被final修饰,因此不可以被继承.
 * String是不变对象,即:对象创建后内容不可改变,若改变字符串内容需要创建新对象
 * String内部使用char数组保存每一个字符.每个char为2个字节,存该字符的unicode编码
 * @author mxy
 */
public class StringDemo {
    public static void main(String[] args) {
        //java推荐使用字面量形式创建字符串对象,并将该对象存入字符串常量池缓存
        String s1 = "123abc";//字面量,直接量
        //当时用已有的字面量再次创建字符串时会冲涌常量池中缓存的该对象
        String s2 = "123abc";//重用s1指向的对象
        System.out.println(s1==s2);//true
        String s3 = "123abc";//依然重用s1指向的对象
        System.out.println(s2==s3);//true
        /*
            new 则强制创建对象
            常见面试题:
            String str = new String("hello");
            上述一句话创建了几个对象?
            答案:2个
            分别是
            1.字面量""hello"会创建一个字符串对象并存入常量池中
            2.new String会创建一个字符串对象只不过内容引用了"hello"对象
         */
        String s4 = new String ("123abc");
        System.out.println("s1:"+s1);
        System.out.println("s4:"+s4);//false
        System.out.println(s1==s4);

        /*
            编译器有一个特性:
            当编译器在编译程序时如果遇到一个计算表达式且计算表达式结果可以在编译期间确定时,
            编译器就会进行计算并将结果编译到class文件中,编译后,下面的代码在字节码文件中的
            样子为:
            String s5="123abc";

         */
        String s5 = "123"+"abc";
        System.out.println("s5:"+s5);
        System.out.println(s1==s5);//true

        String s ="123";
        String s6 = s+"abc";//拼接内容会创建新的字符串对象
        System.out.println("s6:"+s6);
        System.out.println(s1==s6);//false

        s1 = s1+"!";//修改内容会创建新的对象
        System.out.println("s1:"+s1);//123abc
        System.out.println(s1==s2);//false
        System.out.println("s2:"+s2);
        System.out.println("s3:"+s3);

    }

}
