package string;
import java.util.Arrays;
/**
 * String支持的正则表达式的相关方法
 */
public class StringDemo6 {
    public static void main(String[] args) {
        /*
            boolean matches(String regex)
            使用给定的正则表达式验证当前字符串的内容是否符合格式要求
            匹配则返回true否则返回false
         */
        String email = "fancp@tedu.com";
        /*
            邮箱的正则表达式
            [a-zA-Z0-9_]+@[a-zA-Z0-9]+(\.[a-zA-Z]+)+
         */
        String regex = "[a-zA-Z0-9]+@[a-zA-Z0-9]+(\\.[a-zA-z]+)+";

        boolean match = email.matches(regex);
        if(match){
            System.out.println("是邮箱");
        }else{
            System.out.println("NO!");
        }

        String str2 = "abc123def456ghi";
        String[] data = str2.split("[0-9]+");
        System.out.println(data.length);
        System.out.println(Arrays.toString(data));
        String str4 = "abc.def.ghi";
        data = str4.split("\\.");
        System.out.println(Arrays.toString(data));
        String str5 = ",abc,def,ghi";
        data = str5.split(",");
        System.out.println("len:"+data.length);
        System.out.println(Arrays.toString(data));
        String str6 = ",abc,,def,ghi";

        data = str6.split(",");
        System.out.println("len:"+data.length);
        System.out.println(Arrays.toString(data));
        String str7 = "abc,def,ghi,,,,,,,,,,,,";

        data = str7.split(",");
        System.out.println("len:"+data.length);
        System.out.println(Arrays.toString(data));

        /*
            String replaceAll(Strin regex,String str)
         */

        String str8 = "abc123def456ghi";
        str8 = str8.replaceAll("[a-z]+","#NUMBER#");
        System.out.println(str8);

        String reg = "(wqnd|cnm|sb|tmd|djb)";
        String message = "wqnd!你个sb,你会不会?";
        message = message.replaceAll(reg,"***");
        System.out.println(message);
    }
}
