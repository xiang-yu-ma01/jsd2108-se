package string;

public class Tset {
    public static void main(String[] args) {
        String name1 = getHostName("http//www.tedu.cn");
        String name2 = getHostName("doc.anglaoshi.org");
        String name3 = getHostName("www.tarena.com.cn");
        System.out.println(name1);
        System.out.println(name2);
        System.out.println(name3);

    }
    public static String getHostName(String locating){
        //1:先找到locating
        int start = locating.indexOf(".")+1;
        int end = locating.indexOf(".",start);
        //2:
        return locating.substring(start,end);
    }
}

