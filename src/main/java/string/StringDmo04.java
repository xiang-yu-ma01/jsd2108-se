package string;

/**
 * String 提供了一组重载的静态方法valueOf,作用是将其他类型转换为String
 */
public class StringDmo04 {
    public static void main(String[] args) {
        int i = 123;
        String s1 = String.valueOf(i);
        System.out.println(s1);
        double dou = 123.123;
        String s2 = String.valueOf(dou);
        System.out.println(s2);
        //任何内容和字符串连接的结果都是字符串,但是比valueOf性能差
        String s3 = i+"";
        System.out.println(s3);
    }
}
