package string;
import java.util.Scanner;

/**
 * 字符串常见方法2
 */
public class StringDemo03 {
    public static void main(String[] args) {
        //   v(三个空格)  v(三个缩进tab)
        String str = "   HelloWorld         ";
        /*
            String trim()
            去除当前字符串两端的空白字符(回车,换行,空格,缩进....)

         */
        String trim = str.trim();
        System.out.println(str);
        System.out.println(trim);
        /*
            char charAt(int index)
            获取指定位置上的字符
         */
        String str2 = "HelloWorld";
        char c = str2.charAt(5);
        System.out.println(c);//w
        /*
            boolean startsWith(String str);
            判断当前字符串是否是以给定字符串开始的
             boolean endsWith(String str);
            判断当前字符串是否是以给定字符串结尾的

         */

        boolean start = str2.startsWith("H");
        System.out.println(start);
        boolean ends = str2.endsWith("d");
        System.out.println(ends);

        String s1 = "我爱Java";
        String upper = s1.toUpperCase();
        System.out.println("upper:"+upper);
        String lower = s1.toLowerCase();
        System.out.println("lower:"+lower);

        String code ="1H89L";
        System.out.println();
        System.out.println();
        Scanner sac = new Scanner(System.in);
        String input = sac.nextLine();
        if(code.equals(input)){
            System.out.println("正确");
        }else{
            System.out.println("错误");
        }
        if(code.equalsIgnoreCase(input)){
            System.out.println("正确");
        }else{
            System.out.println("错误");
        }


    }

}
