package string;

/**
 * 频繁修改字符串的性能问题
 *
 * 字符串仅适合重用,不适合频繁修改
 */
public class StringDemo05 {
    public static void main(String[] args) {
        String str = "a";
        for(int i = 0;i<1000;i++){
            str+="a";

        }
        System.out.println("完毕");
    }
}
