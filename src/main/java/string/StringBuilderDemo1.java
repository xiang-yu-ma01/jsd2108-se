package string;

/**
 * java.lang.StringBuilder
 * java为了解决修改字符串的性能问题专门提供的字符串修改API
 *
 * StringBuilder内部维护一个可变的char数组，并且提供了便于修改字符串的相关方法:
 * 增删改插等常见操作。
 *
 * StringBuilder修改字符串的性能好，开销小。
 */
public class StringBuilderDemo1 {
    public static void main(String[] args) {
        String trc = "好好学习C语言";
        //默认无参构造创建时,StringBuilder内部表示一个空字符串:"
        // StringBuilder builder= new StringBuilder();
        //基于一个字符串创建StringBuilder,会将给定字符串内容复制一份到StringBuilder
        /*StringBuilder builder = new StringBuilder(trc);*/

        /*
            StringBuffer 是与String一同在jdk1.0出现的
            StringBuilder实在jdk1.5时出现的
            功能一致，但是StringBuffer是并发安全的，StringBuilder则不是。
         */
        StringBuffer builder = new StringBuffer(trc);
        System.out.println("stc:"+trc);
        System.out.println("builder:"+builder);
        /*
            append():将给定的内容追加字符串末尾,等同于字符串的"+"
            好好学习C语言
             v追加
             为了做外挂

         */
        builder.append("为了做外挂");
        System.out.println("builder:"+builder);
        /*
            replace(int start,end,String str)
            将给定内容替换到当前字符串中的部分位置(范围为start-end含头不含尾)
         */
        builder.replace(8,12,"就是为了改变游戏环境");
        System.out.println(builder);
        /*
            delete(int start,int end)
            将指定范围内的字符串删除（两个参数为范围，含头不含尾）
         */
        builder.delete(0,8);
        System.out.println(builder);
        /*
            insert(int offset,String str)
            将指定的字符串添加到指定的位置
         */
        builder.insert(0,"氪金");
        System.out.println(builder);
        /*
            reverse();
            反转字符串
         */
        builder.reverse();
        System.out.println(builder);
        /*
            String toString();
            将StringBuilder内部修饰后的字符串以String形式返回
         */
        String str2 = builder.toString();
        System.out.println("str2:"+str2);

    }
}
