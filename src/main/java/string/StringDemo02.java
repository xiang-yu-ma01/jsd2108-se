package string;

/**
 * String常用方法
 */
public class StringDemo02 {
    public static void main(String[] args) {
        String str = "thinking in java";
        /*
            int length()
            返回当前字符串长度(字符个数)
         */
        int len = str.length();
        System.out.println("长度:"+len);

        /*
            int indexOf(String str)
            检索给定字符传在当前字符串中的起始位置,若返回-1
            说明当前字符串中不包含给定的字符内容

         */
        int index = str.indexOf("in");
        System.out.println("index:"+index);//2

        index = str.indexOf("In");
        System.out.println("index:"+index);//-1
        /*
            int indexOf(String str,int offset)
            从offset处开始检索给定字符串第一次出现的起始位置
         */
        index = str.indexOf("in",3);//从第四个字符开始检索
        System.out.println("index:"+index);
        /*
            int last    IndexOf(String str)
            检索最后一次出现给定字符串的起始位置

         */
        index = str.lastIndexOf("in");
        System.out.println("index:"+index);
        /*
            String substring(int start,int end)
            截取当前字符串中给定范围内的字符串
            在API中通常使用两个数字表示范围时,都是含头不含尾!
         */
        /*
            截取"ing"
         */
        String sub = str.substring(5,8);
        System.out.println(sub);

        /*
            String  substring(int start)
            从start处截取到字符串末尾
         */
        //"in java"
        sub = str.substring(9);
        System.out.println(sub);

    }
}
