package day01;
/**
 * 要求用户在控制台输入自己的用户名。
 * 然后要求做如下验证工作:
 * 1:用户名不能为空(只要有一个字符)
 * 2:用户名必须在20个字以内
 * @author Xiloer
 *
 */
import java.util.Scanner;
public class Test02 {
    public static void main(String[] args) {
        Scanner sca = new Scanner(System.in);
        System.out.println("请输入你的用户名(20字以内):");
        String username = sca.nextLine();
        username = username.trim();
        if(username.length()>=1&&username.length()<=20){
            System.out.println("匹配通过");
        }else{
            System.out.println("匹配不通过");
        }
    }

}
