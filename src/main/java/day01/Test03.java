package day01;
/**
 * 修改下面代码的错误
 * 
 * 下面代码完成的功能是输出字符串中的每一个字符
 * 
 * @author Xiloer
 *
 */
import java.util.Scanner;
public class Test03 {
    public static void main(String[] args) {
        Scanner sca = new Scanner(System.in);
        String str = "hello world! i love java!";
        for(int i = 0;i<str.length();i++) {
            char c =str.charAt(i);
            System.err.println(c);
        }

    }

}






