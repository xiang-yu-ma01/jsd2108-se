package lambda;

import java.io.File;
import java.io.FileFilter;

/**
 * JDK8之后推出的一个新的特性:lambda表达式
 *
 * lambda表达式可以用更精简的语法创建匿名内部类
 * 语法:
 * (参数列表)->{
 *     方法
 * }
 * 需要注意:使用lambda表达式创建的匿名内部类在实现接口时要求该接口只能有一个抽象方法
 * 否则不能用lambda表达式
 */
public class LanbdaDemo {
    public static void main(String[] args) {
        FileFilter filter = new FileFilter(){
            @Override
            public boolean accept(File file){
                return file.getName().startsWith(".");
            }


        };
        //如果参数只有一个"()"可以忽略不写
        FileFilter filter2 = f->{
            return f.getName().startsWith(".");
        };
        /*
            如果方法体中只有一行代码,则方法体{}可以忽略不写
            并且如果这句代码包含return时,return也必须一同忽略.
         */
        //FileFilter fileter3 = f->f.getName().startsWith(".");
        File dir = new File(".");
        File[] subs = dir.listFiles(f->f.getName().startsWith("."));
        for(int i = 0;i<subs.length;i++){
            System.out.println(subs[i].getName());
        }

    }


}
