package file;

import java.io.File;

/**
 * 新建一个目录
 */
public class MkDirDemo {
    public static void main(String[] args) {
        //当前目录下新建一个目录(文件夹):demo
        // File dir = new File("./demo");
        File dir = new File("./a/b/f");
        if(dir.exists()){
            System.out.println("该目录已存在");
        }else{
            /*
                mkdir方法要求创建的目录所在的目录必须存在
                mkdirs则会将所有不存在的父及目录一同创建出来,所有实际开发中常用的方法
             */
            //dir.mkdir();
            dir.mkdirs();
            System.out.println("该目录创建完毕!");
        }
    }
}
