package file;

import java.io.File;
import java.io.FileFilter;

/**
 * File提供了一个重载的 ListFiles方法,允许我们传入一个文件过滤器
 * ,该过滤器可以定义好筛选的条件,因此这个ListFiles方法可以利用该过滤器获取符合筛选条件的子项
 */
public class ListFilesDemo2 {
    public static void main(String[] args) {
        //获取当前目录下的名字"."开始的子项
        File dir = new File(".");
       /* FileFilter filter = new FileFilter(){
            public boolean accept(File file){
                String name = file.getName();
                return name.startsWith(".");
            }
        };*/
        /*
            重载的listFiles会将当前目录中的每一项都经过一次给定的过滤器的accept
            方法,并最终仅将返回值为true的所有子子项存入数组返回.
            注:accept单词的含义为:接受
         */
        File[] subs = dir.listFiles(new FileFilter(){
            public boolean accept(File file){
                String name = file.getName();
                return name.startsWith(".");
            }
        });
        System.out.println("子项一共有"+subs.length+"项");
        for(int i = 0;i<subs.length;i++){
            System.out.println(subs[i].getName());
        }
    }
}
