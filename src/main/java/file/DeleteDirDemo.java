package file;

import java.io.File;

/**
 * 删除一个目录
 */
public class DeleteDirDemo {
    public static void main(String[] args) {
        //当前,目录的demo目录
        File dir = new File("./demo");
        if(dir.exists()){
            //删除目录的前提是必须有一个空目录
            dir.delete();
            System.out.println("已删除!");
        }else{
            System.out.println("该目录不存在");
        }

    }

}
