package file;

import java.io.File;

/**
 * 湖区一个目录的所有子项
 */
public class ListFilesDemo {
    public static void main(String[] args) {
        //获取当前目录中的所有子项
        File dir = new File(".");
        /*
            boolean isFile()


         */
        if(dir.isDirectory()){
            /*
                File[] listFiles()
                将当前File对象表示的目录中的所有子项返回,数组中的每个File表示一个子项
             */
            File[] subs = dir.listFiles();
            System.out.println("当前目录下有"+subs.length+"个子项");
            for(int i= 0;i<subs.length;i++){
                System.out.println(subs[i].getName());
            }
        }
    }
}
