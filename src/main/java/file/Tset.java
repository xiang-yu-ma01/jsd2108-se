package file;

import java.io.File;
import java.io.FileFilter;

/**
 * 随堂练习
 * 获取当前目录中的所有名字含"a"的子项
 */
public class Tset {
    public static void main(String[] args) {
        File dir = new File(".");
        if(dir.isDirectory()){

            File[] subs = dir.listFiles(new FileFilter(){
                public boolean accept(File file){
                    return file.getName().contains("a");
                    //return file.getName().indexOf("a")!=-1;
                    //return name.indexOf("a")>-1;也可以

                }
            } );
            System.out.println(subs.length);
            for(int i = 0;i<subs.length;i++){
                System.out.println(subs[i].getName());
            }
        }

    }
}
