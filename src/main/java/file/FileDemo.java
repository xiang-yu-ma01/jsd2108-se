package file;
import java.io.File;
/**
 * java.io.File
 * File可以表示硬盘上一个文件或者目录(实际保存的是一个抽象路径)
 * 通过File我们可以:
 * 1.访问该文件或者目录的属性信息(名字,大小,最后修改的时间等)
 * 2.创建或者删除文件,目录
 * 3.访问一个目录中的子项
 * 但是不能访问文件数据,有其他API做这个工作
 */
public class FileDemo {
    public static void main(String[] args) {
        /*
            实际开发中我们在指定路径都用相对路径,好处在于更通用,跨平台性好,
            "./"称为当前目录,具体指哪里要看当前程序运行的环境而定,
            例如我们在idea中执行程序时,idea就是我们的运行环境,它定义的"./"为当前
            项目的项目目录
         */

        File file = new File ("./demo.txt");
        //获取名字
        String name = file.getName();
        System.out.println(name);

        //获取绝对路径
        String path = file.getAbsolutePath();
        System.out.println(path);
        //获取大小,单位:字节
        long len = file.length();
        System.out.println("大小"+len+"字节");

        boolean cr = file.canRead();
        boolean cw = file.canWrite();
        boolean ih = file.isHidden();
        System.out.println("是否可读"+cr);
        System.out.println("是否可写"+cw);
        System.out.println("是否隐藏"+ih);
    }


}
