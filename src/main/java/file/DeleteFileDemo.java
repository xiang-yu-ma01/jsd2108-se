package file;
import java.io.File;
/**
 * 将一个文件删除
 */
public class DeleteFileDemo {
    public static void main(String[] args) {
        /*
            在当前目录下的test.txt文件删除
            在相对路径中,最开始的"./"可以忽略不写,默认就是从"./"开始
         */
        File file = new File("test.txt");
        if(file.exists()){
            file.delete();
            System.out.println("该文件已删除!");
        }else{
            System.out.println("该文件不存在!");
        }

    }
}
