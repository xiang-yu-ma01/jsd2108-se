package file;
import java.io.File;
import java.io.IOException;

/**
 * 创建一个新文件
 */
public class CreateNewFileDemo {
    public static void main(String[] args) throws IOException {
        //在当前目录下新建一个新文件:test.txt
        File file = new File("./test.txt");
        if(file.exists()){
            System.out.println("该文件已存在");
        }else{
            file.createNewFile();
            System.out.println("该文件创建成功");
        }


    }

}
